<?php

// FRONT CONTROLLER

// 1. Общие настройки
ini_set('display_errors', 1);
error_reporting(E_ALL);

// 2. Подключение файлов системы
define('ROOT', dirname('__FILE__'));

spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'Engine\\';

    // base directory for the namespace prefix
    $base_dir = __DIR__ . '/engine/';

    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // no, move to the next registered autoloader
        return;
    }

    // get the relative class name
    $relative_class = substr($class, $len);

    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
});

use Engine\builder\queries\TaxonomyFolders;
use Engine\builder\build\BuildProject;
use Engine\builder\build\BuildSection;

$tax = new TaxonomyFolders;
$tax->create_taxonomy_folders();

$build_projects = new BuildProject;
$build_projects->create_files();

$build_sections = new BuildSection;
$build_sections->create_files();


