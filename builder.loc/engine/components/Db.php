<?php

namespace Engine\components;

class Db
{

    public static function getConnection()
    {
        $paramsPath = ROOT . '/engine/config/db_params.php';
        $params = include($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']};charset=utf8";
        set_time_limit(6000000);
        $db = new \PDO($dsn, $params['user'], $params['password']);

        return $db;
    }
}
