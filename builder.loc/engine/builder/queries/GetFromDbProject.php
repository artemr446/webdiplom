<?php

namespace Engine\builder\queries;

use Engine\builder\build\Build;

class GetFromDbProject extends GetFromDb {
	public function get_project_fields($nid)
	{
		$nids = $this->query('SELECT nid from node WHERE type = "project" AND nid = ' . $nid . '');

		$fields = array(
			'title' => $this->query('SELECT title FROM node WHERE nid = ' . $nids[0]['nid']. ''),
			'body_value' => $this->query('SELECT body_value FROM field_data_body WHERE entity_id = ' . $nids[0]['nid'] . ''),
			'course_value' => $this->query('SELECT field_course_value FROM field_data_field_course WHERE entity_id = ' . $nids[0]['nid'] . ''),
			'discipline' => $this->query('SELECT taxonomy_term_data.name
				FROM taxonomy_term_data
				INNER JOIN field_data_field_discipline ON field_data_field_discipline.field_discipline_tid = taxonomy_term_data.tid AND field_data_field_discipline.entity_id = ' . $nids[0]['nid'] . ''),
			'project_type' => $this->query('SELECT taxonomy_term_data.name
				FROM taxonomy_term_data
				INNER JOIN field_data_field_project_type ON field_data_field_project_type.field_project_type_tid = taxonomy_term_data.tid AND field_data_field_project_type.entity_id = ' . $nids[0]['nid'] . ''),
			'field_number' => $this->query('SELECT field_number_value FROM field_data_field_number WHERE entity_id = ' . $nids[0]['nid'] . ''),
			'alias' => $this->query('SELECT alias from url_alias WHERE source = "node/' . $nids[0]['nid'] .'"')
		);

		return($fields);
	}

	public function get_files_list()
	{
		$files_list = $this->query('SELECT nid from node WHERE type = "project"');
		$files = [];
		foreach($files_list as $file)
		{
			$files[$file['nid']] = $file['nid'];
		}
		return $files;
	}
}