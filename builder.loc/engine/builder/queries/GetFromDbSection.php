<?php

namespace Engine\builder\queries;

class GetFromDbSection extends GetFromDb {

	private $discipline_array;

	// форирование списка заголовков
	private function node_links($tid, $title) 
	{
		if(!empty($this->discipline_array[$tid]['titles'])) {
			$array_item = '<h2>' . $title . '</h2><ol>';
			foreach($this->discipline_array[$tid]['titles'] as $title)
			{
				$node_alias = GetFromDb::query('SELECT alias FROM url_alias WHERE source="node/' . $title['nid'] . '"');
				$node_link = explode('/', $node_alias[0]['alias'])[2];
				$array_item .= '<li><a href="' . $node_link . '">' . $title['title'] . '</a></li>';
			}
			$array_item .= '</ol>';
			$this->discipline_array[$tid][$tid] = $array_item;
		} else {
			$array_item = '';
			$this->discipline_array[$tid][$tid] = $array_item;
		}
	}
	

	public function get_section_fields($tid)
	{
		// формирование массива типов работ (дипломная работа, контрольная и т.д)
		$types_array = [];
		$types = GetFromDb::query('SELECT tid,name FROM taxonomy_term_data WHERE vid=3');
		foreach($types as $type)
		{
			$type_alias = GetFromDb::query('SELECT alias FROM url_alias WHERE source="taxonomy/term/' . $type[0] . '"');
			
			$alias = explode('/', $type_alias[0]['alias'])[1];
				
			$types_array[$type[0]]['alias'] = $alias;
			$types_array[$type[0]]['tid'] = $type[0];
			$types_array[$type[0]]['name'] = $type['name'];
		}

		// формирование массива с иерархией:  тип - список нод

		$this->discipline_array = [];

		$discipline_alias = GetFromDb::query('SELECT alias FROM url_alias WHERE source="taxonomy/term/' . $tid . '"');
		$alias = explode('/', $discipline_alias[0]['alias'])[1];
		$discipline_name = $this->query('SELECT name FROM taxonomy_term_data WHERE vid=2 AND tid=' . $tid . '');

		$this->discipline_array['discipline_tid'] = $tid;
		$this->discipline_array['discipline_name'] = $discipline_name[0]['name'];
		$this->discipline_array['discipline_alias'] = $alias;
		foreach($types_array as $type)
		{
			$this->discipline_array[$type['tid']]['titles'] = GetFromDb::query('SELECT title,nid FROM node WHERE nid IN (SELECT entity_id FROM field_data_field_project_type WHERE entity_id IN (SELECT entity_id FROM field_data_field_discipline WHERE field_discipline_tid='. $tid .') AND field_project_type_tid = ' . $type['tid'] . ')');
			$this->discipline_array[$type['tid']]['name'] = $type['name'];
		}
		$this->node_links(56, 'Дипломные работы');
		$this->node_links(55, 'Курсовые работы');
		$this->node_links(54, 'Контрольные работы');
		$this->node_links(57, 'Отчеты по практике');
		$this->node_links(58, 'Задачи');

		return($this->discipline_array);
	}

	public function get_files_list()
	{
		$files_list = $this->query('SELECT tid FROM taxonomy_term_data WHERE vid=2');
		$files = [];
		foreach($files_list as $file)
		{
			$files[$file['tid']] = $file['tid'];
		}
		return $files;
	}
}
