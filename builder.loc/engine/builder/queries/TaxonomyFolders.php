<?php

namespace Engine\builder\queries;

class TaxonomyFolders extends GetFromDb {

	public function create_taxonomy_folders()
	{
		$tax = GetFromDb::query('SELECT alias from url_alias');
		$htaccess = 'RewriteEngine On

		# Remove trailing slash
		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteCond %{REQUEST_URI} (.+)/$
		RewriteRule ^ %1 [R=301,L]

		RewriteCond %{REQUEST_FILENAME} !-d
		RewriteCond %{REQUEST_URI} /(.*)/$
		RewriteRule ^ /%1 [R=301,L]

		# Remove .html
		RewriteCond %{THE_REQUEST} /([^.]+)\.html [NC]
		RewriteRule ^ /%1 [NC,L,R]

		RewriteCond %{REQUEST_FILENAME}.html -f
		RewriteRule ^ %{REQUEST_URI}.html [NC,L]';
		
		foreach($tax as $name) {
			if(strpos($name['alias'], 'katalog/') !== false) {
				$folder_name = explode('/', $name['alias']);
				if(!file_exists('output/katalog/' . $folder_name[1])) {
					mkdir('output/katalog/' . $folder_name[1], 0777, true);
				}
				$filepath = ROOT . '/output/katalog/' . $folder_name[1] . '/.htaccess';
				$file = fopen($filepath, 'w');
				fwrite($file, $htaccess);
				fclose($file);
			}
		}
	}
}
