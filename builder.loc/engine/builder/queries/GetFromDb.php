<?php

namespace Engine\builder\queries;

use Engine\components\Db;

abstract class GetFromDb
{

	protected function query($query)
	{
		$db = Db::getConnection();
		$result = $db->prepare($query);
		$result->execute();
		$result_array = $result->fetchAll();
		return $result_array;
	}
}
