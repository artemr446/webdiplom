<?php

namespace Engine\builder\templates;

class TemplateProject extends Template {

	public function __construct($fields)
	{
		$this->fields = $fields;
	}

	private function description($text, $count=27)
	{
		$words = str_replace('<br />','',$text);
		$words = str_replace('<p>','',$words);
		$words = str_replace('</p>','',$words);
		$words = str_replace('<ul>','',$words);
		$words = str_replace('</ul>','',$words);
		$words = str_replace('<li>','',$words);
		$words = str_replace('</li>','',$words);
		$words = str_replace('&nbsp;','',$words);
	    $words = explode(' ', $words);

	    $result = '';
	    for ($i = 0; $i < $count && isset($words[$i]); $i++) {
	        $result .= ' '. $words[$i];
	    }

	    return $result;
	}

	private function project_template()
	{
		if(isset($this->fields['alias'][0][0])) {
			$predmet = explode('/',$this->fields['alias'][0][0])[1];
		}

		if(isset($this->fields['field_number'][0][0])) {
			$project_number = $this->fields['field_number'][0][0];
		} else {
			$project_number = '';
		}

		if(isset($this->fields['body_value'][0][0])) {
			$content = $this->fields['body_value'][0][0];
		} else if (isset($this->fields['course_value'][0][0])) {
			$content = $this->fields['course_value'][0][0];
		} else {
			$content = '';
		}
		$description_metatag = $this->description($content, $count=30);

		$template = '
		<!DOCTYPE html>
		<html>
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="abstract" content="дипломная курсовая контрольная отчет">
				<meta name="description" content="'. $this->fields['discipline'][0][0] . ' ' . $this->fields['project_type'][0][0] . ' ' . $description_metatag . '"/>
				<title>' . $this->fields['title'][0][0] . '</title>
				' . $this->get_site_icon() . '
				' . $this->get_styles() . '
			</head>

			<body>
				
				' . $this->get_menu() . '
				' . $this->get_search_box() . '
				
				<div class="breadcrumbs">
				    <div class="container">
				    <a href="/">Главная</a>
                    <span>/</span>
                    <a href="/katalog">Каталог</a>
                    <span>/</span>
                    <a href="/katalog/' . $predmet . '">' . $this->fields['discipline'][0][0] . '</a>
                    <span>/</span>
                    <span>' . $this->fields['title'][0][0] .'</span></div>
				</div>

				<div class="project container">
					<div class="content section">
						<h1>' . $this->fields['title'][0][0] . '</h1>
						<div>' . $content . '</div>
						<div>Предмет: <a href="/katalog/' . $predmet . '">' . $this->fields['discipline'][0][0] . '</a></div>
						<div class="field-name-field-project-type">Тип работы: <span>' . $this->fields['project_type'][0][0] . '</span></div>
						<div class="field-name-field-price">Стоимость: <span></span></div>

						<div class="make-order-wrapper clearfix">
							<div class="instructions">
								<p>
									Чтобы заказать работу или задать вопрос, заполните форму обратной связи или позвоните по указанному номеру:
								</p>
								<div class="mobile-contacts-wrapper">
									<p><i class="fas fa-mobile-alt"></i><a href="tel:+375295377790">+ 375 29 53 777 90</a> (МТС, Viber)</p>
									<p><i class="fas fa-mobile-alt"></i><a href="tel:+375291295562">+ 375 29 129 55 62</a> (VELCOM)</p>
								</div>
							</div>
							<form id="form" class="project-form clearfix">
								<div class="inputs">
									<input name="name" placeholder="Имя">
									<input name="phone" placeholder="Телефон">
									<input name="email" placeholder="Email">
									<input name="project-info" placeholder="Email" value="предмет: '. $this->fields['discipline'][0][0] .' , название: '. $this->fields['title'][0][0] .' , номер работы: ' . $project_number . '">
								</div>
								<div class="textarea">
									<textarea name="message" placeholder="Сообщение"></textarea>
								</div>
								<div class="button">
									<button>Отправить</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				' . $this->get_scripts() . '
			</body>

			' . $this->get_footer() . '

		</html> ';

		return $template;
	}

	public function get_project_template()
	{
		$project_template = $this->project_template();
		return $project_template;
	}
}
