<?php

namespace Engine\builder\templates;

class TemplateSection extends Template {

	public function __construct($fields)
	{
		$this->fields = $fields;
	}

	private function section_template()
	{
		
		$template = '<!DOCTYPE html>
					<html>
						<head>
							<meta charset="UTF-8">
							<meta name="abstract" content="дипломная курсовая контрольная отчет">
							<meta name="viewport" content="width=device-width, initial-scale=1">
							<title>' . $this->fields['discipline_name'] . '</title>
							' . $this->get_site_icon() . '
							' . $this->get_styles() . '
						</head>

						<body>
							' . $this->get_menu() . '
							' . $this->get_search_box() . '

							<div class="breadcrumbs">
							    <div class="container">
							    <a href="/">Главная</a>
                                <span>/</span>
                                <a href="/katalog">Каталог</a>
                                <span>/</span>
                                <span>' . $this->fields['discipline_name'] .'</span></div>
							</div>

							<div class="container">
								<h1>' . $this->fields['discipline_name'] . '</h1>
								<div class="content section">

									' . $this->fields[56][56] . '
									' . $this->fields[55][55] . '
									' . $this->fields[54][54] . '
									' . $this->fields[57][57] . '
									' . $this->fields[58][58] . '

								</div>
							</div>
							' . $this->get_scripts() . '
						</body>

						' . $this->get_footer() . '

					</html> ';

		return $template;
	}

	public function get_section_template()
	{
		$section_template = $this->section_template();
		return $section_template;
	}
}