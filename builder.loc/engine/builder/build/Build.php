<?php

namespace Engine\builder\build;

class Build {

	private $fields;

	public function create_files()
	{
		$files = $this->get_files();
		foreach($files as $file) {
			$fields = $this->get_fields($file);
			$template = $this->template($fields);
			$filepath = $this->filepath($fields);
			$file = fopen($filepath, 'w');
			fwrite($file, $template);
			fclose($file);
		}
	}
}