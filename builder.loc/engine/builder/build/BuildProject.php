<?php

namespace Engine\builder\build;

use Engine\builder\templates\TemplateProject;
use Engine\builder\queries\GetFromDbProject;

class BuildProject extends Build {

	public function get_files()
	{
		$project = new GetFromDbProject;
		$files = $project->get_files_list();
		return $files;
	}
	public function get_fields($file)
	{
		$project = new GetFromDbProject;
		$fields = $project->get_project_fields($file);
		return $fields;
	}
	public function filepath($fields)
	{
		$filepath = 'output/' . $fields['alias'][0][0] . '.html';
		return $filepath;
	}

	public function template($fields)
	{
		$template_obj = new TemplateProject($fields);
		$template = $template_obj->get_project_template();
		return $template;
	}
}