<?php

namespace Engine\builder\build;

use Engine\builder\templates\TemplateSection;
use Engine\builder\queries\GetFromDbSection;

class BuildSection extends Build {

	public function get_files()
	{
		$sections = new GetFromDbSection;
		$files = $sections->get_files_list();
		return $files;
	}
	public function get_fields($file)
	{
		$sections = new GetFromDbSection;
		$fields = $sections->get_section_fields($file);
		return $fields;
	}
	public function filepath($fields)
	{
		$filepath = 'output/katalog/' . $fields['discipline_alias'] . '/index.html';
		return $filepath;
	}

	public function template($fields)
	{
		$template_obj = new TemplateSection($fields);
		$template = $template_obj->get_section_template();
		return $template;
	}
}