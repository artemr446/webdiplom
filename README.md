Порядок работы с проектом:
1) Развернуть на локальном сервере diplom.loc (сайт cms drupal);
2) На локальный сервер скпировать builder.loc и diplomstatic.loc;
3) Набрать в адресной строке builder.loc после чего начнется создание папок и файлов в builder.loc/output/katalog
4) Скопировать содержимое папки builder.loc/output/katalog в diplomstatic.loc/katalog
5) Статический сайт готов
Примечания:
1) Редактирование контента происходит на сайте diplom.loc
2) builder.loc и diplom.loc используют одну и ту же базу
3) После внесения изменений повторить путкты 1-5