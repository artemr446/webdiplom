(function($){
	$(document).ready(function(){

		$('#form').submit(function(){

			$.ajax({
				type: "POST",
				url: "../../../mail.php",
				data: $(this).serialize()
			}).done(function(){
				alert("Спасибо за заявку! Скоро мы с Вами свяжемся.");
			});
			return false;
		});
	});

	$(document).ready(function() {
		function switchClass(el, className) {
		if(el.hasClass(className)) {
				el.removeClass(className);
				el.next().removeClass(className);
			} else if (!el.hasClass(className)) {
				el.addClass(className);
				el.next().addClass(className);
			}
		}
	    // open/close mobile menu
	    var switchMenu = $('.main-menu-controls');
	    switchMenu.click(function(){
	    	switchClass($(this), 'opened');
	    	switchClass($('.menu-wrapper'), 'opened');
	    });
	});

	// set products price
  $(document).ready(function() {
    switch ($('.field-name-field-project-type span').text()) {
        case 'Задача': {
          $('.field-name-field-price span').text("3 BYN.");
          break;
        }
        case 'Контрольная работа': {
          $('.field-name-field-price span').text("10 BYN.");
          break;
        }
        case 'Отчет по практике': {
          $('.field-name-field-price span').text("10 BYN.");
          break;
        }
        case 'Курcoвая работа': {
          $('.field-name-field-price span').text("20 BYN.");
          break;
        }  
        case 'Дипломная работа': {
          $('.field-name-field-price span').text("50 BYN.");
          break;
        }  
    }
    
  });

  // front page slider
	$(document).ready(function() {
        var ww = $(window).width();
        var images = ['bgu-1.jpg', 'bgatu-1.jpg', 'bgeu-1.jpg', 'bgsha-1.jpg', 'bgu-2.jpg'];
        if(ww > 768) {
			var i = 0;
            $('.image').append('<img src="images/' + images[i] + '" alt="' + images[i] + '">');
            setInterval(function(){
            	var img = $('.image img');
				if(i >= images.length -1) {
					i = 0;
                    img.attr("src", "images/" + images[i]);
                    img.attr("alt", images[i]);
                    img.fadeTo(0, 0);
                    img.fadeTo(1000, 1);
				} else {
					i++;
                    img.attr("src", "images/" + images[i]);
                    img.attr("alt", images[i]);
                    img.fadeTo(0, 0);
                    img.fadeTo(1000, 1);
				}
            }, 5000);
		}
	});

})(jQuery);
