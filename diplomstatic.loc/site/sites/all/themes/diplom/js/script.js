(function($) {

  // catalog page grid
	$(document).ready(function() {
		var catalogContainerWidth = window.innerWidth;
 
    if(catalogContainerWidth > 992) {
      var divs = $('.view-catalog').children('.view-content').children('.item-list');
      divs.slice(0, 7).wrapAll("<div class='new'></div>");
      divs.slice(7, 13).wrapAll("<div class='new'></div>");
      divs.slice(13, 19).wrapAll("<div class='new'></div>");
    } else if (catalogContainerWidth <= 992) {
      var divs = $('.view-catalog').children('.view-content').children('.item-list');
      divs.slice(0, 9).wrapAll("<div class='new'></div>");
      divs.slice(9, 19).wrapAll("<div class='new'></div>");
    }
    $(window).resize(function() {
      var catalogContainerWidth = window.innerWidth;
      if(catalogContainerWidth > 992) {
        $('.item-list').unwrap();
        var divs = $('.view-catalog').children('.view-content').children('.item-list');
        divs.slice(0, 7).wrapAll("<div class='new'></div>");
        divs.slice(7, 13).wrapAll("<div class='new'></div>");
        divs.slice(13, 19).wrapAll("<div class='new'></div>");
      } else if (catalogContainerWidth <= 992) {
        $('.item-list').unwrap();
        var divs = $('.view-catalog').children('.view-content').children('.item-list');
        divs.slice(0, 9).wrapAll("<div class='new'></div>");
        divs.slice(9, 19).wrapAll("<div class='new'></div>");
      }
    });
	});

  // dropdown filter
  $(document).ready(function() {
    $('select#edit-field-project-type-tid').prettyDropdown({
      height: 50
    });
  });

  //animated text front page
  $(document).ready(function(){
    $('.search-form input').attr('placeholder','Поиск по сайту');
  });

  // set products price
  $(document).ready(function() {
    switch ($('.field-name-field-project-type a').text()) {
        case 'Задача': {
          $('.field-name-field-price .field-item').text("3 руб.");
          break;
        }
        case 'Контрольная работа': {
          $('.field-name-field-price .field-item').text("10 руб.");
          break;
        }
        case 'Отчет по практике': {
          $('.field-name-field-price .field-item').text("10 руб.");
          break;
        }
        case 'Курcoвая работа': {
          $('.field-name-field-price .field-item').text("20 руб.");
          break;
        }  
        case 'Дипломная работа': {
          $('.field-name-field-price .field-item').text("50 руб.");
          break;
        }  
    }
    // add project number to email
    var projrctNum = $('.field-name-field-number .field-item').text();
    $('.webform-component--nomer input').val(projrctNum);
    // add phone number
    $('.mobile-contacts-wrapper').html('<p><i class="fa fa-mobile" aria-hidden="true"></i><a href="tel:+375295377790">+ 375 29 53 777 90</a> (МТС)</p><p><i aria-hidden="true" class="fa fa-mobile"></i><a href="tel:+375291295562">+375 29 129 55 62</a> (VELCOM)</p>');
  });


  // front page ajax slideshow
  $(document).ready(function(){
    var slideshowContainer = $('.slideshow-container');
    $.ajax({
      url: '/json-slideshow',
      success: function(result){
        var bgImages = result.nodes.length;
        var i = 0;
        var timerId = setInterval(function() {
          var imageUrl = result.nodes[i].node.field_slide.src;
          slideshowContainer.css('background-image', 'url("' + imageUrl + '")');
          if(i<bgImages-1) {
            i++;
          } else {
            i=0;
          }
        }, 10000);
      }
    });
  });

  // ask question

  var JSAPP = window.JSAPP || {};

  var body = $('body');

  JSAPP.aqPop = function () {
    $('#ask-question-btn').on('click', function () {
      $('#ask-question-pop').fadeIn();
      return false;
    });
    $('.close-pop').on('click', function () {
      $('#ask-question-pop').fadeOut();
      return false;
    });
  };
  $(document).ready(function () {
    JSAPP.aqPop();
  });

  //   fixed footer

  $(document).ready(
    function() {
      var footer = $('#footer');
      var wh = $(window).height();
      var fo = footer.offset().top;
      var fh = footer.height();
      if(fo-fh < wh) {
        footer.addClass('fixed-footer');
      }
    }
  );

  
  function switchClass(el, className) {
    if(el.hasClass(className)) {
        el.removeClass(className);
        el.next().removeClass(className);
      } else if (!el.hasClass(className)) {
        el.addClass(className);
        el.next().addClass(className);
      }
  }
  $(document).ready(function() {
    // open/close mobile menu
    var switchMenu = $('.main-menu-controls');
    switchMenu.off().on().click(function(){
      switchClass($(this), 'opened');
    });
    // open/close project content
    var showHideContent = $('.views-label-field-course');
    showHideContent.off().on().click(function() {
      switchClass($(this), 'opened');
    });
    // // read more button taxonomy page
    // var showHideContentNext = $(".views-label-field-course");
    // showHideContentNext.off().on().click(function(){
    //   switchClass($(this), 'expanded');
    // });
  });
   
})(jQuery);