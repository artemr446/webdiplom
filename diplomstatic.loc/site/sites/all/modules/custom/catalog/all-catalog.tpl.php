<div id="catalog-app" ng-controller="catalogController">
	<div class="catalog" ng-repeat="catalog in catalog">
		<h2>{{catalog.node.title}}</h2>
		<div>{{catalog.node.body}}</div>
		<div>{{catalog.node.field_course}}</div>
	</div>

	<div class="pagination-centered">
      <uib-pagination total-items="totalItems" ng-model="currentPage" items-per-page="itemsPerPage" ng-change="pageChanged(currentPage)"></uib-pagination>
    </div>
</div>