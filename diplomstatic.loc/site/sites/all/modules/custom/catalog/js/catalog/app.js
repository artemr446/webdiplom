var catalog = angular.module('catalog', ['ui.bootstrap']);

jQuery(document).ready(function(){
	angular.bootstrap(document.getElementById('catalog-app'), ['catalog']);
});