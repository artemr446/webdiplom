catalog.controller('catalogController', function($scope, $http){
	$scope.currentPage = 1;
	$scope.itemsPerPage = 10;

	$http.get('/json/catalog').success(function(result){
		$scope.allCatalog = (function(){
			return result.nodes;
		})();

		$scope.catalog = $scope.allCatalog.slice(0, $scope.itemsPerPage);

		$scope.totalItems = $scope.allCatalog.length;
	});

	$scope.pageChanged = function (currentPage) {
	    var start = (currentPage - 1) * $scope.itemsPerPage;
	    var end = start + $scope.itemsPerPage;
	    $scope.catalog = $scope.allCatalog.slice(start, end);
	  };
});