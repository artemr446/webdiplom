<!DOCTYPE html>
					<html>
						<head>
							<meta charset="UTF-8">
							<meta name="abstract" content="дипломная курсовая контрольная отчет">
							<meta name="description" content=""/>
                            <meta name="viewport" content="width=device-width, initial-scale=1">
							<title>Информация</title>
							<link rel="icon" href="../images/icon/webdiplom.png">
							<link rel="stylesheet" href="../css/style.css">
						</head>

						<body class="informacija">
							<div class="menu-wrapper">
								<div class="container">
									<a href="/" class="menu-logo"><i class="far fa-file-word"></i></i><span>     Дилом24.бел<span></a>
									<div class="main-menu-controls">
										<div class="block-bars"><i class="fas fa-bars"></i></div>
										<div class="block-times"><i class="fas fa-times"></i></div>
									</div>
									<div class="main-menu">
										<ul class="clearfix">
											<li><a href="../katalog">Каталог</a></li>
											<li><span>Информация</span></li>
											<li>
												<div class="mobile-contacts-wrapper">
													<p><i class="fas fa-mobile-alt"></i><a href="tel:+375295377790">+ 375 29 53 777 90</a> (МТС, Viber)</p>
													<p><i class="fas fa-mobile-alt"></i><a href="tel:+375291295562">+375 29 129 55 62</a> (VELCOM)</p>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="search container">
								<gcse:search></gcse:search>
							</div>

							<div class="breadcrumbs">
							    <div class="container">
							        <a href="/">Главная</a>
                                    <span>/</span>
                                    <span>Информация</span>
							    </div>
							</div>

							<div class="project container">
								<div class="content section">
									<h1>Информация</h1>
									<div>
										<p><b>Дилом24.бел</b> УНП 691129478 оказывает информационные и консультационные услуги студентам. Если Вам необходимо собрать информацию по теме или нужна помощь в написании работы целиком – обращайтесь к нам. На сайте <b>Дилом24.бел</b> содержится большое количество готовых контрольных, курсовых и дипломных работ. Все работы были успешно зачтены и одобрены преподавателями.</p>
										<h2>Как заказать работу?</h2>
											<p>Чтобы <a href="/"><b>заказать работу</b></a>, заполните <a href="/"><b>форму обратной связи</b></a> на главной странице, после чего с Вами свяжется специалист.</p>
											<p>Также с нами можно связаться, набрав один из указанных номеров</p>
											<div class="mobile-contacts-wrapper">
												<p><i class="fas fa-mobile-alt"></i><a href="tel:+375295377790">+ 375 29 53 777 90</a> (МТС)</p>
												<p><i class="fas fa-mobile-alt"></i><a href="tel:+375291295562">+375 29 129 55 62</a> (VELCOM)</p>
											</div>
										<h2>Способы оплаты</h2>
										<p>Оплата осуществляется переводом средств на банковский счет</p>
										<h2>Цены на готвые работы</h2>
										<p>
											Задача: 3 BYN</br></br>

											Отчет по практике: 10 BYN</br></br>

											Контрольная работа: 10 BYN</br></br>

											Курсовая работа: 20 BYN</br></br>

											Дипломная работа: 50 BYN</br></br>

											Если проекта по вашей теме не нашлось в каталоге, можно <a href="/"><b>заказать новую работу</b></a>, заполнив форму обратной связи на главной странице
										</p>
										<h2>Дисциплины, по которым можно заказать работу</h2>
										<div class="view-content clearfix">
    <div class="new">
        <div class="item-list">
            <h3>А</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/audit-i-reviziya">Аудит и ревизия</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ahd">АХД</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Б</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/belorusskaya-literatura">Белорусская литература</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/belorusskiy-yazyk">Белорусский язык</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/buhuchet">Бухучет</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>В</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/vneshneekonomicheskaya-deyatelnost">Внешнеэкономическая деятельность</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Д</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/deloproizvodstvo">Делопроизводство</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/dengi-kredit-bank-dkb">Деньги Кредит Банк (ДКБ)</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Ж</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/zhurnalistika">Журналистика</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>З</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/zashchita-naseleniya">Защита населения</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>И</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/innovacii-i-investicii">Инновации и инвестиции</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/istoriya">История</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>К</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/kommercheskaya-deyatelnost">Коммерческая деятельность</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Л</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/logika">Логика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/logistika">Логистика</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>М</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/makroekonomika">Макроэкономика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/marketing">Маркетинг</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/mezhdunarodnaya-ekonomika">Международная экономика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/menedzhment">Менеджмент</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/mirovaya-hudozhestvennaya-kultura">Мировая Художественная Культура</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/mto">МТО</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Н</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/nalogooblozhenie">Налогообложение</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/normirovanie-i-organizaciya-truda">Нормирование и организация труда</a></span>  </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="new">
        
        <div class="item-list">
            <h3>О</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/organizaciya-proizvodstva">Организация производства</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/organizaciya-torgovli">Организация торговли</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ohrana-truda">Охрана труда</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>П</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/planirovanie">Планирование</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/pravo">Право</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/predprinimatelstvo">Предпринимательство</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/proizvodstvennye-tehnologii">Производственные технологии</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/promyshlennyy-servis">Промышленный сервис</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/psihologiya-i-pedagogika">Психология и педагогика</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Р</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ritorika">Риторика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/russkiy-yazyk">Русский язык</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>С</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/selskoe-hozyaystvo">Сельское хозяйство</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/sociologiya">Социология</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/standartizaciya">Стандартизация</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/statistika">Статистика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/strahovanie">Страхование</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Т</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/tekstologiya">Текстология</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/tehnologiya">Технология</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/tovarovedenie">Товароведение</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Ф</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/fizicheskaya-kultura">Физическая культура</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/filosofiya">Философия</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/finansy">Финансы</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Ц</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/cenoobrazovanie">Ценообразование</a></span>  </div>
                </li>
            </ul>
        </div>
        <div class="item-list">
            <h3>Э</h3>
            <ul>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekologiya">Экология</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekonomika">Экономика</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekonomika-apk">Экономика АПК</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekonomika-organizacii">Экономика организации</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekonomicheskaya-teoriya">Экономическая теория</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/ekskursovedenie">Экскурсоведение</a></span>  </div>
                </li>
                <li class="">
                    <div class="views-field views-field-name">        <span class="field-content"><a href="/katalog/etika-i-estetika">Этика и эстетика</a></span>  </div>
                </li>
            </ul>
        </div>
    </div>
</div>
									</div>
								</div>
							</div>

                            <!-- Global site tag (gtag.js) - Google Analytics -->
                            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-99505921-2"></script>
                            <script>
                              window.dataLayer = window.dataLayer || [];
                              function gtag(){dataLayer.push(arguments);}
                              gtag('js', new Date());

                              gtag('config', 'UA-99505921-2');
                            </script>

                            <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
                            <script>
                              (function() {
                                var cx = '007745609872944884378:rgyt_eaazrq';
                                var gcse = document.createElement('script');
                                gcse.type = 'text/javascript';
                                gcse.async = true;
                                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                                var s = document.getElementsByTagName('script')[0];
                                s.parentNode.insertBefore(gcse, s);
                              })();
                            </script>
              
                            <script defer src="https://use.fontawesome.com/releases/v5.4.2/js/all.js" integrity="sha384-wp96dIgDl5BLlOXb4VMinXPNiB32VYBSoXOoiARzSTXY+tsK8yDTYfvdTyqzdGGN" crossorigin="anonymous"></script>

							<script src="../../js/script.js"></script>
						</body>

						<footer>
							<div class="container">
								<a href="/" class="footer-logo"><i class="far fa-file-word"></i></i>     Дилом24.бел</a>
							</div>
						</footer>

					</html> 