<!DOCTYPE html>
					<html>
						<head>
							<meta charset="UTF-8">
							<meta name="abstract" content="дипломная курсовая контрольная отчет">
							<meta name="description" content=""/>
							<title>Контакты</title>
							<link rel="icon" href="../images/icon/webdiplom.png">
							<link rel="stylesheet" href="../css/style.css">
						</head>

						<body>
							<div class="menu-wrapper">
								<div class="container">
									<a href="/" class="menu-logo"><i class="far fa-file-word"></i></i>     Дилом24.бел</a>
									<div class="main-menu-controls">
										<div class="block-bars"><i class="fas fa-bars"></i></div>
										<div class="block-times"><i class="fas fa-times"></i></div>
									</div>
									<div class="main-menu">
										<ul class="clearfix">
											<li><a href="../katalog">Каталог</a></li>
											<li><a href="../informaciya">Информация</a></li>
											<li><span>Контакты</span></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="breadcrumbs container">
								<a href="/">Главная</a>
								<span>/</span>
								<span>Контакты</span>
							</div>

							<div class="project container">
								<div class="content">
									<h1>Контакты</h1>
									<div class="content">

									</div>
								</div>
							</div>

                            <!-- Global site tag (gtag.js) - Google Analytics -->
                            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-99505921-2"></script>
                            <script>
                              window.dataLayer = window.dataLayer || [];
                              function gtag(){dataLayer.push(arguments);}
                              gtag('js', new Date());

                              gtag('config', 'UA-99505921-2');
                            </script>

                            <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>

			  				<script defer src="https://use.fontawesome.com/releases/v5.4.2/js/all.js" integrity="sha384-wp96dIgDl5BLlOXb4VMinXPNiB32VYBSoXOoiARzSTXY+tsK8yDTYfvdTyqzdGGN" crossorigin="anonymous"></script>

							<script src="../../js/script.js"></script>
						</body>

						<footer>
							<div class="container">
								<a href="/" class="footer-logo"><i class="far fa-file-word"></i></i>     Дилом24.бел</a>
							</div>
						</footer>

					</html> 